const FilterButton = ({ label, onClick, selected }) => {
  return (
    <li>
      <button className={selected ? "selected" : ""} onClick={onClick}>
        {label}
      </button>
    </li>
  );
};

export default FilterButton;
