import React, { useState } from "react";

const Form = ({ onAdd }) => {
  const [value, setValue] = useState("");

  const onChange = (evt) => {
    setValue(evt.target.value);
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();
    onAdd(value);
    setValue("");
  };

  return (
    <form onSubmit={handleSubmit}>
      <input
        className="new-todo"
        placeholder="Qu'avez vous à faire ?"
        autoFocus
        onChange={onChange}
        value={value}
      />
      <input className="hidden" type="submit" value="Ajouter" />
    </form>
  );
};

export default Form;
