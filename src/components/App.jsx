import React, { useState } from "react";
import FilterButton from "./FilterButton";
import Form from "./Form";
import TodoItem from "./TodoItem";

const App = () => {
  const [todoList, setTodoList] = useState([
    {
      id: "todo-1",
      name: "Tester React",
      completed: true,
    },
    {
      id: "todo-2",
      name: "Terminer le TP",
      completed: true,
    },
    {
      id: "todo-3",
      name: "Offrir du saucisson au prof",
      completed: false,
    },
  ]);
  const [filter, setFilter] = useState("all");
  const [editing, setEditing] = useState(null);
  const leftTodos = todoList.filter((todo) => !todo.completed).length;

  const handleCheckboxChanged = (id) => {
    setTodoList(
      todoList.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    );
  };

  const handleTextChanged = (id, text) => {
    setTodoList(
      todoList.map((todo) => {
        if (todo.id === id) {
          todo.name = text;
        }
        return todo;
      })
    );
  };

  const handleAdd = (todoName) => {
    const todoId = `todo-${todoList.length + 1}`;
    setTodoList([
      {
        id: todoId,
        name: todoName,
        completed: false,
      },
      ...todoList,
    ]);
  };

  const handleDestroy = (id) => {
    setTodoList(todoList.filter((todo) => todo.id !== id));
  };

  const todoListVisual = todoList
    .map((todo) => (
      <TodoItem
        id={todo.id}
        title={todo.name}
        completed={todo.completed}
        key={todo.id}
        onChange={handleCheckboxChanged}
        onDestroy={handleDestroy}
        editing={editing}
        onEdit={setEditing}
        handleTextChanged={handleTextChanged}
      />
    ))
    .filter((todo) => {
      if (filter === "all") return true;
      if (filter === "active") return !todo.props.completed;
      if (filter === "completed") return todo.props.completed;
      return true;
    });

  return (
    <section className="todoapp">
      <header className="header">
        <h1>Todo</h1>
        <Form onAdd={handleAdd} />
      </header>
      {todoList.length ? (
        <>
          <section className="main">
            <input
              id="toggle-all"
              className="toggle-all"
              type="checkbox"
              checked={todoList.every((todo) => todo.completed)}
              onClick={() => {
                todoList.every((todo) => todo.completed)
                  ? setTodoList(
                      todoList.map((todo) => ({ ...todo, completed: false }))
                    )
                  : setTodoList(
                      todoList.map((todo) => ({ ...todo, completed: true }))
                    );
              }}
            />
            <label htmlFor="toggle-all">Tout compléter</label>
            <ul className="todo-list">{todoListVisual}</ul>
          </section>

          <footer className="footer">
            {/* Ceci devrait être "0 restants" par défaut */}
            <span className="todo-count">
              <strong>{leftTodos}</strong> tâches restantes
            </span>
            <ul className="filters">
              <FilterButton
                label={"Tous"}
                selected={filter === "all"}
                onClick={() => {
                  setFilter("all");
                }}
              />
              <FilterButton
                label={"Actifs"}
                selected={filter === "active"}
                onClick={() => {
                  setFilter("active");
                }}
              />
              <FilterButton
                label={"Complétés"}
                selected={filter === "completed"}
                onClick={() => {
                  setFilter("completed");
                }}
              />
            </ul>
            {/* Caché si aucun élément complété restant */}
            <button
              className="clear-completed"
              onClick={() => {
                setTodoList(todoList.filter((todo) => !todo.completed));
              }}
            >
              Effacer les complétés
            </button>
          </footer>
        </>
      ) : (
        <></>
      )}
    </section>
  );
};

export default App;
