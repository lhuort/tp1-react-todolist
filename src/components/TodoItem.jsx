import React, { useState } from "react";

const TodoItem = ({
  title,
  completed,
  id,
  onChange,
  onDestroy,
  editing,
  onEdit,
  handleTextChanged,
}) => {
  const [text, setText] = useState(title);

  const textChanged = (evt) => {
    evt.preventDefault();
    handleTextChanged(id, text);
    onEdit(null);
  };

  return (
    <li className={editing === id ? "editing" : completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          id={id}
          checked={completed}
          onChange={() => {
            onChange(id);
          }}
        />
        <label
          onDoubleClick={() => {
            onEdit(id);
          }}
        >
          {title}
        </label>
        <button
          className="destroy"
          onClick={() => {
            onDestroy(id);
          }}
        />
      </div>
      <form onBlur={textChanged} onSubmit={textChanged}>
        <input
          className="edit"
          defaultValue={title}
          onChange={(evt) => {
            setText(evt.target.value);
          }}
        />
        <input type="submit" value="Valider" className="hidden" />
      </form>
    </li>
  );
};

export default TodoItem;
